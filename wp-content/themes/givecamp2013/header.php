<?php
/**
 * Header Template
 *
 *
 * @file        header.php
**/

if ( ! defined ( 'ABSPATH' ) )
{
	header ( "HTTP/1.0 403 Forbidden" );
	require_once ( $_SERVER [ 'DOCUMENT_ROOT' ] . '/wp-blog-header.php' );
	die ( 'Direct file access prohibited.  Please go back to the <a href="' . site_url () . '">homepage</a> and try again.' );
}

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<?php
$htmlClass = 'page-inside';
$logoUrl = home_url ();
if ( is_front_page () )
{
	$htmlClass = 'page-home';
  $logoUrl = '#';
}
?>
<html <?php language_attributes(); ?>  class="no-js <?php echo $htmlClass; ?>">
<!--<![endif]-->
<head>
<?php
$options = get_option ( 'bw_responsive_theme_options' );
$sep = '';
if ( isset ( $options [ 'title_separator' ] ) && $options [ 'title_separator' ] != NULL )
{
	$sep = $options [ 'title_separator' ];
}
else
{
	$sep = ' | ';
}
?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<title><?php wp_title (); ?><?php echo $sep; ?><?php bloginfo ( 'name' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/webfonts/fontawesome/fontawesome.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/socialmediaicons.css" />
  <!-- Detect Javascript -->
  <script>document.documentElement.className = document.documentElement.className.replace("no-js","js");</script>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/css3-mediaqueries.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <!--
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/chosen/chosen.jquery.min.js"></script>
  -->
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>

        <!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
	<?php wp_head(); ?>
</head>
<body>
    <div class="page-wrapper">

        <header role="banner">

          <div class="top-bar container">
            <a class="logo" href="<?php echo $logoUrl; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-clswm.png" alt="Community Legal Services of West Michigan"></a>
            <?php get_search_form(); ?>
          </div>

          <span id="menu-trigger">Menu</span>
          <nav>
              <div class="container">
              <?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'depth' => '1' ) ); ?>
              </div>
          </nav>

        </header>
