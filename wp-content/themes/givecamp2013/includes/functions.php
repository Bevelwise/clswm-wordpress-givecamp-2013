<?php
if ( !defined('ABSPATH')) exit;

remove_action ( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

add_action('after_setup_theme', 'givecamp2013_setup');

if (!function_exists('givecamp2013_setup')):
	function givecamp2013_setup()
	{
		global $content_width;
		if (!isset($content_width))
		{
			$content_width = 550;
		}
		load_theme_textdomain('givecamp2013', get_template_directory().'/languages');

		$locale = get_locale();
		$locale_file = get_template_directory().'/languages/$locale.php';
		if (is_readable( $locale_file))
		{
			require_once( $locale_file);
		}
		add_editor_style('editor-style.css');
		add_theme_support('automatic-feed-links');
		add_theme_support('post-thumbnails');
		function remove_menus ()
		{
		global $menu;
			$restricted = array( __('Links'), __('Comments'));
			end ($menu);
			while (prev($menu))
			{
				$value = explode(' ',$menu[key($menu)][0]);
				if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
			}
		}
		add_action('admin_menu', 'remove_menus');
		register_nav_menus(array(
			'main-menu'	  	=> __('Main Menu', 'givecamp2013'),
			'footer-menu'	  	=> __('Footer Menu', 'givecamp2013')
			)
		);
		if ( function_exists('get_custom_header'))
		{
			add_theme_support('custom-background');
		}
		else
		{
			add_custom_background();
		}
		if (function_exists('get_custom_header'))
		{
			add_theme_support('custom-header', array (
			'default-image'		=> get_template_directory_uri() . '/images/default-logo.png',
			'header-text'		=> false,
			'flex-width'		=> true,
			'width'			=> 300,
			'flex-height'		=> true,
			'height'		=> 100,
			'admin-head-callback'	=> 'givecamp2013_admin_header_style'));

			function givecamp2013_admin_header_style()
			{
			?>
				<style type="text/css">
					.appearance_page_custom-header #headimg {
						background-repeat:no-repeat;
						border:none;
					}
				 </style>
			<?php
			}

		}
		else
		{
			define('HEADER_TEXTCOLOR', '');
			define('HEADER_IMAGE', '%s/images/default-logo.png');
			define('HEADER_IMAGE_WIDTH', 300);
			define('HEADER_IMAGE_HEIGHT', 100);
			define('NO_HEADER_TEXT', true);

			function givecamp2013_admin_header_style()
			{
				?><style type="text/css">
					#headimg {
						background-repeat:no-repeat;
						border:none !important;
						width:<?php echo HEADER_IMAGE_WIDTH; ?>px;
						height:<?php echo HEADER_IMAGE_HEIGHT; ?>px;
					}
				 </style><?php
			}
		 	add_custom_image_header('', 'givecamp2013_admin_header_style');
		}
	}

endif;

function givecamp2013_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'givecamp2013_page_menu_args' );

function givecamp2013_wp_page_menu ($page_markup)
{
	preg_match('/^<div class=\"([a-z0-9-_]+)\">/i', $page_markup, $matches);
	$divclass = $matches[1];
	$replace = array('<div class="'.$divclass.'">', '</div>');
	$new_markup = str_replace($replace, '', $page_markup);
	$new_markup = preg_replace('/^<ul>/i', '<ul class="'.$divclass.'">', $new_markup);
	return $new_markup;
}
add_filter('wp_page_menu', 'givecamp2013_wp_page_menu');

function givecamp2013_comment_count( $count ) {
	if ( ! is_admin() ) {
		global $id;
		$comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
		return count($comments_by_type['comment']);
	} else {
		return $count;
	}
}
add_filter('get_comments_number', 'givecamp2013_comment_count', 0);

function givecamp2013_comment_list_pings( $comment ) {
	$GLOBALS['comment'] = $comment;
?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php }

function givecamp2013_excerpt_length($length) {
	return 15;
}

add_filter('excerpt_length', 'givecamp2013_excerpt_length');

function new_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter('excerpt_more', 'new_excerpt_more');


function givecamp2013_remove_gallery_css($css) {
	return preg_replace("#<style type='text/css'>(.*?)</style>#s", '', $css);
}

add_filter('gallery_style', 'givecamp2013_remove_gallery_css');

function givecamp2013_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'givecamp2013_remove_recent_comments_style' );

if ( ! function_exists( 'givecamp2013_post_meta_data' ) ) :

function givecamp2013_post_meta_data() {
	printf( __( '<span class="%1$s">Posted on </span>%2$s<span class="%3$s"> by </span>%4$s', 'givecamp2013' ),
	'meta-prep meta-prep-author posted',
	sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="timestamp">%3$s</span></a>',
		get_permalink(),
		esc_attr( get_the_time() ),
		get_the_date()
	),
	'byline',
	sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
		get_author_posts_url( get_the_author_meta( 'ID' ) ),
		sprintf( esc_attr__( 'View all posts by %s', 'givecamp2013' ), get_the_author() ),
		get_the_author()
		)
	);
}
endif;

function givecamp2013_category_rel_removal ($output) {
	$output = str_replace(' rel="category tag"', '', $output);
	return $output;
}

add_filter('wp_list_categories', 'givecamp2013_category_rel_removal');
add_filter('the_category', 'givecamp2013_category_rel_removal');

function givecamp2013_breadcrumb_lists ()
{
	$chevron = '<span class="chevron">&#8250;</span>';
	$home = __('Home','givecamp2013');
	$before = '<span class="breadcrumb-current">';
	$after = '</span>';

	if ( !is_home() && !is_front_page() || is_paged() )
	{
		echo '<div class="breadcrumb-list">';
		global $post;
		$homeLink = home_url();
		echo '<a href="' . $homeLink . '">' . $home . '</a> ' . $chevron . ' ';

		if ( is_category() )
		{
			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$thisCat = $cat_obj->term_id;
			$thisCat = get_category($thisCat);
			$parentCat = get_category($thisCat->parent);
			if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $chevron . ' '));
			echo $before; printf( __( 'Archive for %s', 'givecamp2013' ), single_cat_title('', false) ); $after;
 		}
 		elseif ( is_day() )
 		{
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $chevron . ' ';
			echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $chevron . ' ';
			echo $before . get_the_time('d') . $after;
		}
		elseif ( is_month() )
		{
			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $chevron . ' ';
			echo $before . get_the_time('F') . $after;
 		}
 		elseif ( is_year() )
 		{
	  		echo $before . get_the_time('Y') . $after;
 		}
 		elseif ( is_single() && !is_attachment() )
 		{
	  		if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a> ' . $chevron . ' ';
				echo $before . get_the_title() . $after;
			}
			else
			{
				$cat = get_the_category(); $cat = $cat[0];
				echo get_category_parents($cat, TRUE, ' ' . $chevron . ' ');
				echo $before . get_the_title() . $after;
			}

		}
		elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() )
		{
			$post_type = get_post_type_object(get_post_type());
			echo $before . $post_type->labels->singular_name . $after;
		}
		elseif ( is_attachment() )
		{
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo get_category_parents($cat, TRUE, ' ' . $chevron . ' ');
			echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> ' . $chevron . ' ';
			echo $before . get_the_title() . $after;
		}
		elseif ( is_page() && !$post->post_parent )
		{
			echo $before . get_the_title() . $after;
		}
		elseif ( is_page() && $post->post_parent )
		{
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id)
			{
				$page = get_page($parent_id);
				$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
				$parent_id  = $page->post_parent;
		  	}
			$breadcrumbs = array_reverse($breadcrumbs);
			foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $chevron . ' ';
			echo $before . get_the_title() . $after;
		}
		elseif ( is_search() )
		{
			echo $before; printf( __( 'Search results for: %s', 'givecamp2013' ), get_search_query() ); $after;
		}
		elseif ( is_tag() )
		{
			echo $before; printf( __( 'Posts tagged %s', 'givecamp2013' ), single_tag_title('', false) ); $after;
		}
		elseif ( is_author() )
		{
			global $author;
			$userdata = get_userdata($author);
			echo $before; printf( __( 'View all posts by %s', 'givecamp2013' ), $userdata->display_name ); $after;
		}
		elseif ( is_404() )
		{
			echo $before . __('Error 404 ','givecamp2013') . $after;
		}

		if ( get_query_var('paged') )
		{
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
		  	echo __('Page','givecamp2013') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}
		echo '</div>';
	}
}
if (!is_admin())
{
	add_action('wp_enqueue_scripts', 'givecamp2013_js');
}
if (!function_exists('givecamp2013_js')) {
	function givecamp2013_js() {
		//wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/givecamp2013-modernizr.js', array('jquery'), '1.0.0', false);
		//wp_enqueue_script('plugins', get_template_directory_uri() . '/js/givecamp2013-plugins.js', array('jquery'), '1.0.0', true);
		//wp_enqueue_script('scripts', get_template_directory_uri() . '/js/givecamp2013-scripts.js', array('jquery'), '1.0.0', true);
	}

}
function givecamp2013_enqueue_comment_reply()
{
	if ( is_singular() && comments_open() && get_option('thread_comments'))
	{
		wp_enqueue_script('comment-reply');
	}
}
add_action( 'wp_enqueue_scripts', 'givecamp2013_enqueue_comment_reply' );

function givecamp2013_widgets_init()
{
	register_sidebar(array(
		'name' 		=> __('Main Sidebar', 'givecamp2013'),
		'description' 	=> __('Subpage Sidebar - sidebar.php', 'givecamp2013'),
		'id' 		=> 'main-sidebar',
		'before_title' 	=> '<div class="widget-title">',
		'after_title' 	=> '</div>',
		'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
		'after_widget' 	=> '</div>'
	));
	register_sidebar(array(
		'name' 		=> __('Blog Sidebar', 'givecamp2013'),
		'description' 	=> __('Blog Sidebar - sidebar-blog.php', 'givecamp2013'),
		'id' 		=> 'blog-sidebar',
		'before_title' 	=> '<div class="widget-title">',
		'after_title' 	=> '</div>',
		'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
		'after_widget' 	=> '</div>'
	));
	register_sidebar(array(
		'name' 		=> __('Home  Widget', 'givecamp2013'),
		'description' 	=> __('Home Widget - in sidebar-home-cta.php', 'givecamp2013'),
		'id' 		=> 'home-cta',
		'before_title' 	=> '<div class="widget-title">',
		'after_title' 	=> '</div>',
		'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
		'after_widget' 	=> '</div>'
	));
	register_sidebar(array(
		'name' 		=> __('Footer Widget', 'givecamp2013'),
		'description' 	=> __('Footer Widget - in footer.php', 'givecamp2013'),
		'id' 		=> 'footer-sidebar',
		'before_title' 	=> '<div class="widget-title">',
		'after_title' 	=> '</div>',
		'before_widget' => '<div id="%1$s" class="widget-wrapper %2$s">',
		'after_widget' 	=> '</div>'
	));
	register_sidebar(array(
		'name' 		=> __('Social Icons', 'givecamp2013'),
		'description' 	=> __('Social Icons - in footer.php', 'givecamp2013'),
		'id' 		=> 'social-icons',
		'before_title' 	=> '<div class="widget-title">',
		'after_title' 	=> '</div>',
		'before_widget' => '',
		'after_widget' 	=> ''
	));
}
add_action('widgets_init', 'givecamp2013_widgets_init');
function bw_insertPage($atts, $content = null)
{
	$output = NULL;
	extract(shortcode_atts(array("page" => ''), $atts));
	if (!empty($page))
	{
		$pageData 	= get_page($page);
		$output 	= $pageData->post_content;
	}
	return $output;
}
add_shortcode('bw_insert', 'bw_insertPage');
add_action('admin_head','admin_css');
function admin_css()
{
	$current_user = wp_get_current_user();
	if (!$current_user->ID == 1)
	{
		echo '<style>';
		echo '.update_nag{display:none}';
		echo '</style>';
	}
}

class givecamp_walker_nav_menu extends Walker_Nav_Menu
{
	var $tree_type = array( 'post_type', 'taxonomy', 'custom' );
	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	function start_lvl( &$output, $depth ) {
		$output .= "\n<ul>\n";
	}

	function end_lvl( &$output, $depth ) {
		$output .= "</ul>\n";
	}

	function start_el( &$output, $item, $depth, $args ) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = ' class="' . esc_attr( $class_names ) . '"';
		if ( $class_names = ' class=""' ) {
			$class_names = '';
		}

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li>';

		$attributes  = ! empty( $item -> attr_title ) ? ' title="'  . esc_attr ( $item -> attr_title ) .'"' : '';
		$attributes .= ! empty( $item -> target		) ? ' target="' . esc_attr ( $item -> target	 ) .'"' : '';
		$attributes .= ! empty( $item -> xfn		) ? ' rel="'	. esc_attr ( $item -> xfn		) .'"' : '';
		$attributes .= ! empty( $item -> url		) ? ' href="'   . esc_attr ( $item -> url		) .'"' : '';

		$item_output = $args -> before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args -> link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args -> after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el(&$output, $item, $depth) {
		$output .= "</li>\n";
	}
}
?>
