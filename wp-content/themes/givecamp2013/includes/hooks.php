<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Theme's Action Hooks
 *
 */
?>
<?php

/**
 * Just after opening <body> tag
 *
 * @see header.php
 */
function givecamp2013_container() {
    do_action('givecamp2013_container');
}

/**
 * Just after closing </div><!-- end of #container -->
 *
 * @see footer.php
 */
function givecamp2013_container_end() {
    do_action('givecamp2013_container_end');
}

/**
 * Just after opening <div id="container">
 *
 * @see header.php
 */
function givecamp2013_header() {
    do_action('givecamp2013_header');
}

/**
 * Just after opening <div id="header">
 *
 * @see header.php
 */
function givecamp2013_in_header() {
    do_action('givecamp2013_in_header');
}

/**
 * Just after closing </div><!-- end of #header -->
 *
 * @see header.php
 */
function givecamp2013_header_end() {
    do_action('givecamp2013_header_end');
}

/**
 * Just before opening <div id="wrapper">
 *
 * @see header.php
 */
function givecamp2013_wrapper() {
    do_action('givecamp2013_wrapper');
}

/**
 * Just after opening <div id="wrapper">
 *
 * @see header.php
 */
function givecamp2013_in_wrapper() {
    do_action('givecamp2013_in_wrapper');
}

/**
 * Just after closing </div><!-- end of #wrapper -->
 *
 * @see header.php
 */
function givecamp2013_wrapper_end() {
    do_action('givecamp2013_wrapper_end');
}

/**
 * Just before opening <div id="widgets">
 *
 * @see sidebar.php
 */
function givecamp2013_widgets() {
    do_action('givecamp2013_widgets');
}

/**
 * Just after closing </div><!-- end of #widgets -->
 *
 * @see sidebar.php
 */
function givecamp2013_widgets_end() {
    do_action('givecamp2013_widgets_end');
}

/**
 * Theme Options
 *
 * @see theme-options.php
 */
function givecamp2013_theme_options() {
    do_action('givecamp2013_theme_options');
}


?>