<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Theme Options
 */
?>
<?php
add_action('admin_init', 'givecamp2013_theme_options_init');
add_action('admin_menu', 'givecamp2013_theme_options_add_page');


/**
 * A safe way of adding JavaScripts to a WordPress generated page.
 */
function givecamp2013_admin_enqueue_scripts( $hook_suffix ) {
	wp_enqueue_style('givecamp2013-theme-options', get_template_directory_uri() . '/includes/theme-options.css', false, '1.0');
	wp_enqueue_script('givecamp2013-theme-options', get_template_directory_uri() . '/includes/theme-options.js', array('jquery'), '1.0');
	wp_enqueue_script('jquery');  
	wp_enqueue_script('thickbox');
	wp_enqueue_style('thickbox');
	wp_enqueue_script('media-upload');
}
add_action('admin_print_styles-appearance_page_theme_options', 'givecamp2013_admin_enqueue_scripts');

/**
 * Init plugin options to white list our options
 */
function givecamp2013_theme_options_init() {
	register_setting('givecamp2013_options', 'givecamp2013_theme_options', 'givecamp2013_theme_options_validate');
}

/**
 * Load up the menu page
 */
function givecamp2013_theme_options_add_page() {
	add_theme_page(__('Theme Options', 'givecamp2013'), __('Theme Options', 'givecamp2013'), 'edit_theme_options', 'theme_options', 'givecamp2013_theme_options_do_page');
}



/**
 * Site Verification and Webmaster Tools
 * If user sets the code we're going to display meta verification
 * And if left blank let's not display anything at all in case there is a plugin that does this
 */
 
function givecamp2013_google_verification() {
	$options = get_option('givecamp2013_theme_options');
	if (!empty($options['google_site_verification'])) {
		echo '<meta name="google-site-verification" content="' . $options['google_site_verification'] . '" />' . "\n";
	}
}

add_action('wp_head', 'givecamp2013_google_verification');

function givecamp2013_bing_verification() {
	$options = get_option('givecamp2013_theme_options');
	if (!empty($options['bing_site_verification'])) {
		echo '<meta name="msvalidate.01" content="' . $options['bing_site_verification'] . '" />' . "\n";
	}
}

add_action('wp_head', 'givecamp2013_bing_verification');

function givecamp2013_pinterest_verification() {
	$options = get_option('givecamp2013_theme_options');
	if (!empty($options['pinterest_site_verification'])) {
		echo '<meta name="p:domain_verify" content="' . $options['pinterest_site_verification'] . '" />' . "\n";
	}
}

add_action('wp_head', 'givecamp2013_pinterest_verification');

function givecamp2013_google_analytics_script() {
	$options = get_option('givecamp2013_theme_options');
	if (!empty($options['google_analytics_script'])) {
		echo $options['google_analytics_script'];
	}
}

add_action('wp_head', 'givecamp2013_google_analytics_script');

function givecamp2013_inline_css() {
	$options = get_option('givecamp2013_theme_options');
	if (!empty($options['givecamp2013_inline_css'])) {
		echo '<!-- Custom CSS Styles -->' . "\n";
		echo '<style type="text/css" media="screen">' . "\n";
		echo $options['givecamp2013_inline_css'] . "\n";
		echo '</style>' . "\n";
	}
}

add_action('wp_head', 'givecamp2013_inline_css');

function givecamp2013_inline_js_head() {
	$options = get_option('givecamp2013_theme_options');
	if (!empty($options['givecamp2013_inline_js_head'])) {
		echo '<!-- Custom Scripts -->' . "\n";
		echo $options['givecamp2013_inline_js_head'];
		echo "\n";
	}
}

add_action('wp_head', 'givecamp2013_inline_js_head');

function givecamp2013_inline_js_footer() {
	$options = get_option('givecamp2013_theme_options');
	if (!empty($options['givecamp2013_inline_js_footer'])) {
		echo '<!-- Custom Scripts -->' . "\n";
		echo $options['givecamp2013_inline_js_footer'];
		echo "\n";
	}
}

add_action('wp_footer', 'givecamp2013_inline_js_footer');
	
/**
 * Create the options page
 */
function givecamp2013_theme_options_do_page() {

	if (!isset($_REQUEST['settings-updated']))
		$_REQUEST['settings-updated'] = false;
	?>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
	<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
	<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; }
	#sortable li {/* margin: 0 5px 5px 5px;*/ padding: 5px; /*font-size: 1.2em;*/ height: 1.5em; cursor:move; }
	html>body #sortable li { height: 1.5em; line-height: 1.2em; }
	.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
	</style>
	<div class="wrap">
		<?php
		/**
		 * < 3.4 Backward Compatibility
		 */
		?>
		<?php $theme_name = function_exists('wp_get_theme') ? wp_get_theme() : get_current_theme(); ?>
		<?php //screen_icon(); 
		echo "<h2>GiveCamp2013 ". __('Theme Options', 'givecamp2013') . "</h2>"; 
		$current_user = wp_get_current_user();
		$user = $current_user->ID;
		?>

		

		<?php if (false !== $_REQUEST['settings-updated']) : ?>
		<div class="updated fade"><p><strong><?php _e('Options Saved', 'givecamp2013'); ?></strong></p></div>
		<?php endif; ?>
		
		
		<form method="post" action="options.php">
			<?php settings_fields('givecamp2013_options'); ?>
			<?php $options = get_option('givecamp2013_theme_options'); ?>
			
			
			<h3 class="rwd-toggle"><a href="#"><?php _e('Theme Elements', 'givecamp2013'); ?></a></h3>
			<div class="rwd-container">
				<div class="rwd-block"> 
					
							   
				<?php
				/**
				 * Breadcrumb Lists
				 */
				?>
				<div class="grid col-300"><?php _e('Disable Breadcrumb Lists?', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
									<input id="givecamp2013_theme_options[breadcrumb]" name="givecamp2013_theme_options[breadcrumb]" type="checkbox" value="1" <?php isset($options['breadcrumb']) ? checked( '1', $options['breadcrumb'] ) : checked('0', '1'); ?> />
									<label class="description" for="givecamp2013_theme_options[breadcrumb]"><?php _e('Check to disable', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
			
				<?php
				/**
				 * CTA Button
				 */
				?>
				<div class="grid col-300"><?php _e('Disable Call to Action Button?', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
									<input id="givecamp2013_theme_options[cta_button]" name="givecamp2013_theme_options[cta_button]" type="checkbox" value="1" <?php isset($options['cta_button']) ? checked( '1', $options['cta_button'] ) : checked('0', '1'); ?> />
									<label class="description" for="givecamp2013_theme_options[cta_button]"><?php _e('Check to disable', 'givecamp2013'); ?></label>
						
					</div><!-- end of .grid col-620 -->
			
				<?php
				/**
				 * Title Tag Separator
				 */
				?>
					<div class="grid col-300"><?php _e('Title Tag Separator', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[title_separator]" class="regular-text" type="text" name="givecamp2013_theme_options[title_separator]" value="<?php if (!empty($options['title_separator'])) echo esc_attr($options['title_separator']); ?>" />
						<label class="description" for="givecamp2013_theme_options[title_separator]"><?php _e('Used in Site Title Tag', 'givecamp2013'); ?></label>
						<p class="submit">
						<input type="submit" class="button-primary" value="<?php _e('Save Options', 'givecamp2013'); ?>" />
						</p>
					</div><!-- end of .grid col-620 -->
			
				
									
				</div><!-- end of .rwd-block -->
			</div><!-- end of .rwd-container -->
			
			<h3 class="rwd-toggle"><a href="#"><?php _e('Company Information', 'givecamp2013'); ?></a></h3>
			<div class="rwd-container">
				<div class="rwd-block"> 
					<?php
					/**
					* Default Contact Information
					*/
					?>
					<div class="grid col-300"><?php _e('Address', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[address]" class="regular-text" type="text" name="givecamp2013_theme_options[address]" value="<?php if (!empty($options['address'])) echo esc_attr($options['address']); ?>" />
						<label class="description" for="givecamp2013_theme_options[address]"><?php _e('Enter your address', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
			
					<div class="grid col-300"><?php _e('Address 2', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[address2]" class="regular-text" type="text" name="givecamp2013_theme_options[address2]" value="<?php if (!empty($options['address2'])) echo esc_attr($options['address2']); ?>" />
						<label class="description" for="givecamp2013_theme_options[address2]"><?php _e('Address 2', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
			
					<div class="grid col-300"><?php _e('City', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[city]" class="regular-text" type="text" name="givecamp2013_theme_options[city]" value="<?php if (!empty($options['city'])) echo esc_attr($options['city']); ?>" />
						<label class="description" for="givecamp2013_theme_options[city]"><?php _e('Enter your city', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
			
					<div class="grid col-300"><?php _e('State', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[state]" class="regular-text" type="text" name="givecamp2013_theme_options[state]" value="<?php if (!empty($options['state'])) echo esc_attr($options['state']); ?>" />
						<label class="description" for="givecamp2013_theme_options[state]"><?php _e('Enter your state', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
			
					<div class="grid col-300"><?php _e('Zip Code', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[zip]" class="regular-text" type="text" name="givecamp2013_theme_options[zip]" value="<?php if (!empty($options['zip'])) echo esc_attr($options['zip']); ?>" />
						<label class="description" for="givecamp2013_theme_options[zip]"><?php _e('Enter your zip', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
			
					<div class="grid col-300"><?php _e('Phone #', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[phone]" class="regular-text" type="text" name="givecamp2013_theme_options[phone]" value="<?php if (!empty($options['phone'])) echo esc_attr($options['phone']); ?>" />
						<label class="description" for="givecamp2013_theme_options[phone]"><?php _e('Enter your phone', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
			
					<div class="grid col-300"><?php _e('Fax #', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[fax]" class="regular-text" type="text" name="givecamp2013_theme_options[fax]" value="<?php if (!empty($options['fax'])) echo esc_attr($options['fax']); ?>" />
						<label class="description" for="givecamp2013_theme_options[fax]"><?php _e('Enter your fax', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
							   
				
			
				
									
				</div><!-- end of .rwd-block -->
			</div><!-- end of .rwd-container -->
			
			<h3 class="rwd-toggle"><a href="#"><?php _e('Logo Upload', 'givecamp2013'); ?></a></h3>
			<div class="rwd-container">
				<div class="rwd-block">
				<?php
				/**
				 * Logo Upload
				 */
				?>
				<div class="grid col-300"><?php _e('Custom Header', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						
						<p><?php printf(__('Need to replace or remove default logo?','givecamp2013')); ?> <?php printf(__('<a href="%s">Click here</a>.', 'givecamp2013'), admin_url('themes.php?page=custom-header')); ?></p>
					 			
					</div><!-- end of .grid col-620 -->
					
				</div><!-- end of .rwd-block -->
			</div><!-- end of .rwd-container -->
			
			<h3 class="rwd-toggle"><a href="#"><?php _e('Social Icons', 'givecamp2013'); ?></a></h3>
			<div class="rwd-container">
				<div class="rwd-block"> 
					<p>Drag and drop to change the order that each icon will display on the front end.</p>
					<p style="text-align:right;">Active?</p>
				 <ul id="sortable">		   
				<?php
				/**
				 * Social Media
				 */
				$socialArray = array(
					'facebook_uid'	  => $options['social']['facebook_uid'],
					'twitter_uid'	   => $options['social']['twitter_uid'],
					'linkedin_uid'	  => $options['social']['linkedin_uid'],
					'youtube_uid'	   => $options['social']['youtube_uid'],
					'stumble_uid'	   => $options['social']['stumble_uid'],
					'rss_uid'		   => $options['social']['rss_uid'],
					'google_plus_uid'   => $options['social']['google_plus_uid'],
					'instagram_uid'	 => $options['social']['instagram_uid'],
					'pinterest_uid'	 => $options['social']['pinterest_uid'],
					'yelp_uid'		  => $options['social']['yelp_uid'],
					'vimeo_uid'		 => $options['social']['vimeo_uid'],
					'foursquare_uid'	=> $options['social']['foursquare_uid']
					);
				if ($options['social'])
				{
					$newSocialArray = array_merge($options['social'], $socialArray);
				}
				else
				{
					$newSocialArray = $socialArray;
				}
				if (is_array($newSocialArray))
				{
					$icon = '';
					$active = '';
					$title = '';
					$image = '';
					foreach ($newSocialArray as $key => $data)
					{
						$keys[] = $key;
						$parts = explode('_',$key);
						$name = $parts[0];
						$title = ucfirst($name);
						$image = get_stylesheet_directory_uri().'/icons/'.$name.'-icon.png';
						if (isset($data['icon']) && $data['icon'] != '')
						{
							$icon = $data['icon'];
						}
						else
						{
							$icon = $image;
						}
						if (isset($data['active']) && $data['active'] == 1)
						{
							$checked = 'checked="checked"';
							$activeTitle = "Deactivate?";
						}
						else
						{
							$checked = '';
							$activeTitle = 'Activate?';
						}
						echo <<<HTML
						<li class="ui-state-default" id="ordinal_1">
						<div class="grid col-60 fit" style="position:relative;">
							<div id="{$name}_image_holder" style="display:none;"></div>
							<img src="$icon" width="15" height="15" alt="$name" title="Click to update this icon" id="{$name}_icon_holder" class="icon_holder" />
							<input type="text" name="givecamp2013_theme_options[social][$key][icon]" value="{$data['icon']}" id="{$name}_icon" rel="$name" class="icon_upload" style="position:absolute; z-index:10; left:20px; display:none;" />
							<input id="{$name}" class="upload_button" type="button" value="Upload" style="position:absolute; z-index:10; top:-4px; left:155px; display:none;" />
						</div>
						<div class="grid col-300">$title</div><!-- end of .grid col-300 -->
							<div class="grid col-540 fit">
								<input id="givecamp2013_theme_options[social][$key][url]" class="regular-text" type="text" name="givecamp2013_theme_options[social][$key][url]" value="{$data['url']}" />
								<label class="description" for="givecamp2013_theme_options[social][$key][url]">Enter your $title url</label>
			
							</div>
						<div class="squaredOne">
							<input type="checkbox" value="1" $checked style="display:none;" id="givecamp2013_theme_options[social][$key][active]" name="givecamp2013_theme_options[social][$key][active]" />
							<label for="givecamp2013_theme_options[social][$key][active]"></label>
						</div>
						</li>
						
HTML;
					}
				}
				else
				{
				}
				?>
					</ul>
				<p class="submit">
						<input type="submit" class="button-primary" value="<?php _e('Save Options', 'givecamp2013'); ?>" />
						</p>
				
				</div><!-- end of .rwd-block -->
			</div><!-- end of .rwd-container -->
			
			<?php if ($current_user->ID == 1): ?>
			
			<h3 class="rwd-toggle"><a href="#"><?php _e('SEO Tools', 'givecamp2013'); ?></a></h3>
			<div class="rwd-container">
				<div class="rwd-block"> 

							   
				<?php
				/**
				 * Google Site Verification
				 */
				?>
				<div class="grid col-300"><?php _e('Google Site Verification', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[google_site_verification]" class="regular-text" type="text" name="givecamp2013_theme_options[google_site_verification]" value="<?php if (!empty($options['google_site_verification'])) echo esc_attr($options['google_site_verification']); ?>" />
						<label class="description" for="givecamp2013_theme_options[google_site_verification]"><?php _e('Enter your Google ID number only', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
				
				<?php
				/**
				 * Bing Site Verification
				 */
				?>
				<div class="grid col-300"><?php _e('Bing Site Verification', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[bing_site_verification]" class="regular-text" type="text" name="givecamp2013_theme_options[bing_site_verification]" value="<?php if (!empty($options['bing_site_verification'])) echo esc_attr($options['bing_site_verification']); ?>" />
						<label class="description" for="givecamp2013_theme_options[bing_site_verification]"><?php _e('Enter your Bing ID number only', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
				
				<?php
				/**
				 * Pinterest Site Verification
				 */
				?>
				<div class="grid col-300"><?php _e('Pinterest Site Verification', 'givecamp2013'); ?></div><!-- end of .grid col-300 -->
					<div class="grid col-620 fit">
						<input id="givecamp2013_theme_options[pinterest_site_verification]" class="regular-text" type="text" name="givecamp2013_theme_options[pinterest_site_verification]" value="<?php if (!empty($options['pinterest_site_verification'])) echo esc_attr($options['pinterest_site_verification']); ?>" />
						<label class="description" for="givecamp2013_theme_options[pinterest_site_verification]"><?php _e('Enter your Pinterest ID number only', 'givecamp2013'); ?></label>
					</div><!-- end of .grid col-620 -->
				   
				<?php
				/**
				 * Site Statistics Tracker
				 */
				?>
				<div class="grid col-300">
								<?php _e('Google Analytics Script', 'givecamp2013'); ?>
					<span class="info-box information help-links"><?php _e('Leave blank if plugin handles your webmaster tools', 'givecamp2013'); ?></span>
				</div><!-- end of .grid col-300 -->
					
					<div class="grid col-620 fit">
						<textarea id="givecamp2013_theme_options[google_analytics_script]" class="large-text" cols="50" rows="10" name="givecamp2013_theme_options[google_analytics_script]"><?php if (!empty($options['google_analytics_script'])) echo esc_textarea($options['google_analytics_script']); ?></textarea>
						<label class="description" for="givecamp2013_theme_options[google_analytics_script]"><?php _e('Site Stats Script Area - Google Analytics, StatCounter, any other or all of them.  This will be added to the end of the head.', 'givecamp2013'); ?></label>
						
					</div><!-- end of .grid col-620 -->
				<div class="grid col-300">
								<?php _e('Remarketing Script', 'givecamp2013'); ?>
					<span class="info-box information help-links"><?php _e('Leave blank if plugin handles your webmaster tools', 'givecamp2013'); ?></span>
				</div><!-- end of .grid col-300 -->
					
					<div class="grid col-620 fit">
						<textarea id="givecamp2013_theme_options[remarketing_script]" class="large-text" cols="50" rows="10" name="givecamp2013_theme_options[remarketing_script]"><?php if (!empty($options['remarketing_script'])) echo esc_textarea($options['remarketing_script']); ?></textarea>
						<label class="description" for="givecamp2013_theme_options[remarketing_script]"><?php _e('This is a site-wide addition to the end of the body tag.', 'givecamp2013'); ?></label>
						<p class="submit">
						<input type="submit" class="button-primary" value="<?php _e('Save Options', 'givecamp2013'); ?>" />
						</p>
					</div><!-- end of .grid col-620 -->
				
				</div><!-- end of .rwd-block -->
			</div><!-- end of .rwd-container -->
		   
			
			
			
			<h3 class="rwd-toggle"><a href="#"><?php _e('Custom CSS Styles', 'givecamp2013'); ?></a></h3>
			<div class="rwd-container">
				<div class="rwd-block"> 
			
				<?php
				/**
				 * Custom Styles
				 */
				?>
				<div class="grid col-300">
								<?php _e('Custom CSS Styles', 'givecamp2013'); ?>
				</div><!-- end of .grid col-300 -->
				
					<div class="grid col-620 fit">
						<textarea id="givecamp2013_theme_options[givecamp2013_inline_css]" class="inline-css large-text" cols="50" rows="30" name="givecamp2013_theme_options[givecamp2013_inline_css]"><?php if (!empty($options['givecamp2013_inline_css'])) echo esc_textarea($options['givecamp2013_inline_css']); ?></textarea>
						<label class="description" for="givecamp2013_theme_options[givecamp2013_inline_css]"><?php _e('Enter your custom CSS styles.', 'givecamp2013'); ?></label>
						<p class="submit">
						<input type="submit" class="button-primary" value="<?php _e('Save Options', 'givecamp2013'); ?>" />
						</p>
					</div><!-- end of .grid col-620 -->
									
				</div><!-- end of .rwd-block -->
			</div><!-- end of .rwd-container -->
			
			<h3 class="rwd-toggle"><a href="#"><?php _e('Custom Scripts', 'givecamp2013'); ?></a></h3>
			<div class="rwd-container">
				<div class="rwd-block"> 
			
				<?php
				/**
				 * Custom Styles
				 */
				?>
				<div class="grid col-300">
								<?php _e('Custom Scripts for Header and Footer', 'givecamp2013'); ?>
					
				</div><!-- end of .grid col-300 -->
				
					<div class="grid col-620 fit">
						<p><?php printf(__('Embeds to header.php &darr;','givecamp2013')); ?></p>
						<textarea id="givecamp2013_theme_options[givecamp2013_inline_js_head]" class="inline-css large-text" cols="50" rows="30" name="givecamp2013_theme_options[givecamp2013_inline_js_head]"><?php if (!empty($options['givecamp2013_inline_js_head'])) echo esc_textarea($options['givecamp2013_inline_js_head']); ?></textarea>
						<label class="description" for="givecamp2013_theme_options[givecamp2013_inline_js_head]"><?php _e('Enter your custom header script.', 'givecamp2013'); ?></label>
						
						<p><?php printf(__('Embeds to footer.php &darr;','givecamp2013')); ?></p>
						<textarea id="givecamp2013_theme_options[givecamp2013_inline_js_footer]" class="inline-css large-text" cols="50" rows="30" name="givecamp2013_theme_options[givecamp2013_inline_js_footer]"><?php if (!empty($options['givecamp2013_inline_js_footer'])) echo esc_textarea($options['givecamp2013_inline_js_footer']); ?></textarea>
						<label class="description" for="givecamp2013_theme_options[givecamp2013_inline_js_footer]"><?php _e('Enter your custom footer script.', 'givecamp2013'); ?></label>
						<p class="submit">
						<input type="submit" class="button-primary" value="<?php _e('Save Options', 'givecamp2013'); ?>" />
						</p>
					</div><!-- end of .grid col-620 -->
									
				</div><!-- end of .rwd-block -->
			</div><!-- end of .rwd-container -->
			
			 <h3 class="rwd-toggle"><a href="#"><?php _e('Developer', 'givecamp2013'); ?></a></h3>
			<div class="rwd-container">
				<div class="rwd-block"> 
					
					<h3>Complete options array:</h3>
					<?php echo '<pre>'; print_r($options); echo '</pre>'; ?>
					<?php 
						echo "<h3>Theme Functions</h3>";
						echo '<pre>[bw_insert page="XXX"] where XXX = page id will insert the content from one page onto another.</pre>';
					?>
					
				</div>
			</div>
			<?php endif; ?><!-- end of .grid col-940 -->
		</form>
	</div>
	<?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function givecamp2013_theme_options_validate($input) {

	// checkbox value is either 0 or 1
	foreach (array(
		'breadcrumb',
		'cta_button'
		) as $checkbox) 
	{
		if (!isset($input[$checkbox]))
			$input[$checkbox] = null;
			$input[$checkbox] = ( $input[$checkbox] == 1 ? 1 : 0 );
	}

	if (isset($input['home_headline'])) { $input['home_headline'] = wp_kses_stripslashes($input['home_headline']); }
	if (isset($input['home_subheadline'])) {$input['home_subheadline'] = wp_kses_stripslashes($input['home_subheadline']); }
	if (isset($input['home_content_area'])) {$input['home_content_area'] = wp_kses_stripslashes($input['home_content_area']); }
	if (isset($input['cta_text'])) {$input['cta_text'] = wp_kses_stripslashes($input['cta_text']); }
	if (isset($input['cta_url'])) {$input['cta_url'] = esc_url_raw($input['cta_url']); }
	if (isset($input['featured_content'])) {$input['featured_content'] = wp_kses_stripslashes($input['featured_content']); }
	if (isset($input['title_separator'])) {$input['title_separator'] = wp_kses_stripslashes($input['title_separator']); }
	$input['google_site_verification'] = wp_filter_post_kses($input['google_site_verification']);
	$input['bing_site_verification'] = wp_filter_post_kses($input['bing_site_verification']);
	$input['pinterest_site_verification'] = wp_filter_post_kses($input['pinterest_site_verification']);
	$input['google_analytics_script'] = wp_kses_stripslashes($input['google_analytics_script']);
	$input['remarketing_script'] = wp_kses_stripslashes($input['remarketing_script']);
	if (isset($input['twitter_uid'])) {$input['twitter_uid'] = esc_url_raw($input['twitter_uid']); }
	if (isset($input['facebook_uid'])) {$input['facebook_uid'] = esc_url_raw($input['facebook_uid']); }
	if (isset($input['linkedin_uid'])) {$input['linkedin_uid'] = esc_url_raw($input['linkedin_uid']); }
	if (isset($input['youtube_uid'])) {$input['youtube_uid'] = esc_url_raw($input['youtube_uid']); }
	if (isset($input['stumble_uid'])) {$input['stumble_uid'] = esc_url_raw($input['stumble_uid']); }
	if (isset($input['rss_uid'])) {$input['rss_uid'] = esc_url_raw($input['rss_uid']); }
	if (isset($input['google_plus_uid'])) {$input['google_plus_uid'] = esc_url_raw($input['google_plus_uid']); }
	if (isset($input['instagram_uid'])) {$input['instagram_uid'] = esc_url_raw($input['instagram_uid']); }
	if (isset($input['pinterest_uid'])) {$input['pinterest_uid'] = esc_url_raw($input['pinterest_uid']); }
	if (isset($input['yelp_uid'])) {$input['yelp_uid'] = esc_url_raw($input['yelp_uid']); }
	if (isset($input['vimeo_uid'])) {$input['vimeo_uid'] = esc_url_raw($input['vimeo_uid']); }
	if (isset($input['foursquare_uid'])) {$input['foursquare_uid'] = esc_url_raw($input['foursquare_uid']); }
	$input['givecamp2013_inline_css'] = wp_kses_stripslashes($input['givecamp2013_inline_css']);
	$input['givecamp2013_inline_js_head'] = wp_kses_stripslashes($input['givecamp2013_inline_js_head']);
	if (isset($input['givecamp2013_inline_css_js_footer'])) {$input['givecamp2013_inline_css_js_footer'] = esc_url_raw($input['givecamp2013_inline_css_js_footer']); }

	
	return $input;
}