<?php get_header(); ?>

<div class="container motherlode">
	<main>
		<?php if ( have_posts() ): ?>
			<?php while ( have_posts() ): the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			<?php endwhile; ?>
		<?php endif; ?>
	</main>
	<aside>
		<?php get_sidebar(); ?>
	</aside>
</div>
<?php get_footer(); ?>