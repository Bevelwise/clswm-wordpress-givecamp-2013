<form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">

    <input type="text" class="field" name="s" id="s" placeholder="search here &hellip;" style="display:none;"/>
    <input type="submit" class="submit button" name="submit" id="searchsubmit" value="Go"  style="display:none;"/>
    <a href="#" onclick="jQuery('#searchform .field').animate({width: 'toggle'});" class="icon_search"></a>
</form>
