<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 *  Open Content Template
 *
   Template Name:  Open Content
 *
 * @file        sidebar-content-page.php
 */
?>
<?php get_header(); ?>
<div class="container motherlode">
    <main>
        <?php if ( have_posts() ): ?>
        	<?php while ( have_posts() ): the_post(); ?>
        		<h1><?php the_title(); ?></h1>
        		<?php the_content(); ?>
        	<?php endwhile; ?>
        <?php endif; ?>
    </main>
</div>
<?php get_footer(); ?>