<?php get_header(); ?>
<div class="container motherlode">
    <main>
        <h1><?php echo 'Search results for: <span>' . get_search_query() . '</span>'; ?></h1>
        <p>
        <?php if ( have_posts() ): ?>
            <?php while ( have_posts() ): the_post(); ?>
                <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                <?php the_excerpt (); ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </p>
    </main>
    <aside>
        <?php get_sidebar (); ?>
    </aside>
</div>
<div class="cta">
  <div class="container">
    <?php
            if ( is_active_sidebar ( 'home-cta' ) ) //check to see if there's a widget in the home-cta sidebar
            {
                dynamic_sidebar ( 'home-cta' ); //output the widgets in the sidebar
            }
            ?>
  </div>
</div>
<?php get_footer(); ?>