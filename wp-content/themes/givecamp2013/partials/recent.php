<?php 
$args = array (
	'post_type' 		=> 'post',
	'posts_per_page' 	=> 3
);

$home_query = new WP_Query( $args );

if ( $home_query -> have_posts () ) :
	while ( $home_query -> have_posts () ) : $home_query -> the_post (); 
?>
		<div>
			<a href="<?php the_permalink(); ?>">
				<?php
				if ( has_post_thumbnail () ) 
				{
					the_post_thumbnail ();
				}
				else
				{
					echo '<img src="http://fillmurray.com/300/200">';
				}
				?>
				<h2>
					<?php the_title ();  ?>
				</h2>
				<?php the_excerpt (); ?>
			</a>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_postdata (); ?>