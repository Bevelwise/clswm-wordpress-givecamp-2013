<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>

<script src="/wp-content/themes/givecamp2013/js/jquery.cycle.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($)
{
	$('.cycle-slideshow').cycle();
});
</script>

<div class="banner-images slider cycle-slideshow" style="height:450px; width:100%;">
	<?php get_template_part ( 'partials/slider' ); ?>
</div><!-- .slider.banner-images -->
<div class="dark cta">
	<div class="container">
		<?php
		if ( is_active_sidebar ( 'home-cta' ) ) //check to see if there's a widget in the home-cta sidebar
		{
			dynamic_sidebar ( 'home-cta' );	//output the widgets in the sidebar
		}
		?>
	</div>
</div>
<article class="mission">
  <div class="container">
  	<h1><?php the_title(); ?></h1>
	<?php
	if ( has_post_thumbnail ( ) )
	{
		the_post_thumbnail ( 'medium' , array ( 'class' => 'alignright' ) );		// output the image
	}
	the_content ( );					//output the home page content
	?>

  </div>
</article>
<div class="testimonials slider">
	<?php
	if ( shortcode_exists ( 'quoteRotator' ) ) //check to see if quoteRotator shortcode is available
	{
		echo do_shortcode ( '[quoteRotator]' );	//output the quotes
	}
	?>
</div><!-- .testimonials.slider -->
<aside class="post-summaries dark">
  <div class="container">
	<h1>CLS News</h1>
	<div class="columns three-wide">
		<?php get_template_part ( 'partials/recent' ); ?>
    </div>
    <a class="button" href="<?php echo get_page_link ( get_option ( 'page_for_posts' ) ); ?>">More News</a>
  </div>
</aside>
<?php get_footer (); ?>